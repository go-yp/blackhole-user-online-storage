MYSQL_DSN="tester:secret@tcp(localhost:3306)/boutique?multiStatements=true"

up:
	sudo docker-compose up -d

down:
	sudo docker-compose down --volumes

sequence-test:
	MODE=sequence go test ./... -v -bench=. -benchmem -benchtime=10s

parallel-test:
	MODE=parallel go test ./... -v -bench=. -benchmem -benchtime=10s

MIGRATE_NAME ?= undefined
migrate-create:
	mkdir -p ./storage/schema
	goose -dir ./storage/schema -table schema_migrations mysql $(MYSQL_DSN) create $(MIGRATE_NAME) sql

migrate-up:
	goose -dir ./storage/schema -table schema_migrations mysql $(MYSQL_DSN) up
migrate-redo:
	goose -dir ./storage/schema -table schema_migrations mysql $(MYSQL_DSN) redo
migrate-down:
	goose -dir ./storage/schema -table schema_migrations mysql $(MYSQL_DSN) down
migrate-reset:
	goose -dir ./storage/schema -table schema_migrations mysql $(MYSQL_DSN) reset
migrate-status:
	goose -dir ./storage/schema -table schema_migrations mysql $(MYSQL_DSN) status

generate-dbs:
	docker run --rm -v $(shell pwd):/src -w /src kjconroy/sqlc generate