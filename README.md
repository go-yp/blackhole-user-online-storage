### Blackhole user online storage

##### DevOps actions
```sql
SHOW CREATE TABLE user_online;
SHOW CREATE TABLE user_online_blackhole;

SELECT COUNT(*) FROM user_online;

CREATE TABLE user_online_blackhole
(
    user_id INT NOT NULL,
    online  INT NOT NULL
) ENGINE = BLACKHOLE;

RENAME TABLE user_online TO user_online_innodb, user_online_blackhole TO user_online;
```

##### Benchmark result for INSERT ON DUPLICATE KEY UPDATE
####### ENGINE = InnoDB
```bash
make sequence-test
```
```text
BenchmarkUpsertStorage
    storage_test.go:30: b.N =        1; COUNT(*) =        1;
    storage_test.go:30: b.N =      100; COUNT(*) =      100;
    storage_test.go:30: b.N =     2224; COUNT(*) =     2224;
BenchmarkUpsertStorage-12    	    2224	   5550465 ns/op	     269 B/op	      12 allocs/op
```
```bash
make parallel-test
```
```text
BenchmarkUpsertStorage
    storage_test.go:30: b.N =        1; COUNT(*) =        1;
    storage_test.go:30: b.N =      100; COUNT(*) =      100;
    storage_test.go:30: b.N =    10000; COUNT(*) =    10000;
    storage_test.go:30: b.N =    15258; COUNT(*) =    15258;
BenchmarkUpsertStorage-12    	   15258	    736141 ns/op	    1247 B/op	      19 allocs/op
```
####### ENGINE = BLACKHOLE
```bash
make sequence-test
```
```text
BenchmarkUpsertStorage
    storage_test.go:30: b.N =        1; COUNT(*) =        0;
    storage_test.go:30: b.N =      100; COUNT(*) =        0;
    storage_test.go:30: b.N =     4076; COUNT(*) =        0;
BenchmarkUpsertStorage-12    	    4076	   2569420 ns/op	     270 B/op	      12 allocs/op
```
```bash
make parallel-test
```
```text
BenchmarkUpsertStorage
    storage_test.go:30: b.N =        1; COUNT(*) =        0;
    storage_test.go:30: b.N =      100; COUNT(*) =        0;
    storage_test.go:30: b.N =    10000; COUNT(*) =        0;
    storage_test.go:30: b.N =    31562; COUNT(*) =        0;
BenchmarkUpsertStorage-12    	   31562	    371916 ns/op	     979 B/op	      18 allocs/op
```

##### Benchmark result for UPDATE
####### Populate before test
```sql
SET cte_max_recursion_depth = 100000;
INSERT INTO users(id)
WITH RECURSIVE sequence AS (
    SELECT (SELECT COUNT(*) FROM users) + 1 AS level
    UNION
    SELECT sequence.level + 1 AS level
    FROM sequence
    WHERE sequence.level > (SELECT COUNT(*) FROM users)
      AND sequence.level < ((SELECT COUNT(*) FROM users) + 100000)
)
SELECT level
FROM sequence;
SELECT COUNT(*)
FROM users;
INSERT IGNORE INTO user_online(user_id, online)
SELECT id AS user_id,
       0  AS online
FROM users;
```
####### ENGINE = InnoDB
```bash
make sequence-test
```
```text
BenchmarkUpdateStorage
BenchmarkUpdateStorage-12    	    1982	   5560056 ns/op	     268 B/op	      12 allocs/op
```
```bash
make parallel-test
```
```text
BenchmarkUpdateStorage
BenchmarkUpdateStorage-12    	   14839	    801739 ns/op	    1129 B/op	      18 allocs/op
```
####### ENGINE = BLACKHOLE
```bash
make sequence-test
```
```text
BenchmarkUpdateStorage
BenchmarkUpdateStorage-12    	  185133	     62295 ns/op	     271 B/op	      12 allocs/op
```
```bash
make parallel-test
```
```text
BenchmarkUpdateStorage
BenchmarkUpdateStorage-12    	  572284	     25105 ns/op	     555 B/op	      15 allocs/op
```