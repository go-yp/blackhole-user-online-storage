package storage

import (
	"context"
	"gitlab.com/go-yp/blackhole-user-online-storage/storage/dbs"
)

type OnlineUpdateStorage struct {
	queries *dbs.Queries
}

func NewOnlineUpdateStorage(queries *dbs.Queries) *OnlineUpdateStorage {
	return &OnlineUpdateStorage{queries: queries}
}

func (s *OnlineUpdateStorage) Store(userID int, timestamp int) error {
	return s.queries.UserOnlineUpdate(context.Background(), dbs.UserOnlineUpdateParams{
		UserID: int32(userID),
		Online: int32(timestamp),
	})
}
