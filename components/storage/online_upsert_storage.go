package storage

import (
	"context"
	"gitlab.com/go-yp/blackhole-user-online-storage/storage/dbs"
)

type OnlineUpsertStorage struct {
	queries *dbs.Queries
}

func NewOnlineUpsertStorage(queries *dbs.Queries) *OnlineUpsertStorage {
	return &OnlineUpsertStorage{queries: queries}
}

func (s *OnlineUpsertStorage) Store(userID int, timestamp int) error {
	return s.queries.UserOnlineUpsert(context.Background(), dbs.UserOnlineUpsertParams{
		UserID: int32(userID),
		Online: int32(timestamp),
	})
}
