package storage

type Storage interface {
	Store(userID int, timestamp int) error
}
