package storage

import (
	"context"
	"database/sql"
	"gitlab.com/go-yp/blackhole-user-online-storage/storage/dbs"
	"os"
	"sync/atomic"
	"testing"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/stretchr/testify/require"
)

func BenchmarkUpdateStorage(b *testing.B) {
	connection, err := sql.Open("mysql", "tester:secret@tcp(localhost:3306)/boutique")
	require.NoError(b, err)
	defer connection.Close()

	queries, err := dbs.Prepare(context.Background(), connection)
	require.NoError(b, err)
	defer queries.Close()

	benchmarkStorage(b, NewOnlineUpdateStorage(queries))
}

func BenchmarkUpsertStorage(b *testing.B) {
	connection, err := sql.Open("mysql", "tester:secret@tcp(localhost:3306)/boutique")
	require.NoError(b, err)
	defer connection.Close()

	queries, err := dbs.Prepare(context.Background(), connection)
	require.NoError(b, err)
	defer queries.Close()

	benchmarkStorage(b, NewOnlineUpsertStorage(queries))
}

func benchmarkStorage(b *testing.B, storage Storage) {
	b.Helper()

	if os.Getenv("MODE") == "parallel" {
		benchmarkStorageParallel(b, storage)
	} else {
		benchmarkStorageSequence(b, storage)
	}
}

func benchmarkStorageSequence(b *testing.B, storage Storage) {
	b.Helper()

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		var (
			userID    = i
			timestamp = i
		)

		err := storage.Store(userID, timestamp)

		require.NoError(b, err)
	}

	b.StopTimer()
}

func benchmarkStorageParallel(b *testing.B, storage Storage) {
	b.Helper()

	b.ResetTimer()

	// starts from 1 as starts AUTO_INCREMENT
	var (
		counter = uint32(1)
		now     = uint32(time.Now().Unix())
	)

	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			var index = atomic.AddUint32(&counter, 1)

			var (
				userID    = int(index)
				timestamp = int(now + index)
			)

			err := storage.Store(userID, timestamp)

			require.NoError(b, err)
		}
	})

	b.StopTimer()
}
