-- name: UserOnlineUpsert :exec
INSERT INTO user_online (user_id, online)
VALUES (?, ?)
ON DUPLICATE KEY
    UPDATE online = VALUES(online);

-- name: UserOnlineUpdate :exec
UPDATE user_online
SET online = ?
WHERE user_id = ?;