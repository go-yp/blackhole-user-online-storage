-- +goose Up
-- +goose StatementBegin
CREATE TABLE users
(
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT
    -- name
    -- and other columns
) ENGINE = InnoDB;

CREATE TABLE user_online
(
    user_id INT NOT NULL PRIMARY KEY REFERENCES users (id),
    online  INT NOT NULL
) ENGINE = InnoDB;
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE user_online;
DROP TABLE users;
-- +goose StatementEnd
